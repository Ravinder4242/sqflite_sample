import 'package:sqflite_sample/BaseTable.dart';
import 'package:sqflite_sample/model/Dog.dart';
import 'package:synchronized/synchronized.dart';

class DogTable extends BaseTable<Dog> {
  DogTable sInstance;
  final _lock = Lock();

  final String tableName = "dog_table";
  final String columnId = "id";
  final String columnName = "name";

  Future<DogTable> getInstance() async {
    await _lock.synchronized(() async {
      if (sInstance == null) {
        sInstance = new DogTable();
      }
    });
    return sInstance;
  }

  @override
  Map<String, String> getFields() {
    Map<String, String> map = {
      columnId: fieldType(FieldType.integer),
      columnName: fieldType(FieldType.text),
    };
    return map;
  }

  @override
  int getId(Dog t) {
    return t.id;
  }

  @override
  String getTableName() {
    return tableName;
  }

  @override
  Map<String, dynamic> toMap(Dog t) {
    return t.toMap();
  }

  @override
  Dog fromMap(Map<String, dynamic> json) {
    return Dog.fromMap(json);
  }



}
