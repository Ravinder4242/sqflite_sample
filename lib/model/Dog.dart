class Dog {
  final int id;
  final String name;

  Dog({this.id, this.name,
  });

  factory Dog.fromMap(Map<String, dynamic> json) => new Dog(
    id: json["id"],
    name: json["name"],
  );

  // Convert a Dog into a Map. The keys must correspond to the names of the
  // columns in the database.
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
    };
  }

  // Implement toString to make it easier to see information about
  // each dog when using the print statement.
  @override
  String toString() {
    return 'Dog{id: $id, name: $name}';
  }
}