import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite_sample/DogTable.dart';

class DBHelper {

  static final _databaseName = "dog.db";
  static final _databaseVersion = 1;

  DBHelper();

  // make this a singleton class
  DBHelper._privateConstructor();
  static final DBHelper instance = DBHelper._privateConstructor();

  // only have a single app-wide reference to the database
  static Database _database;
  Future<Database> get database async {
    if (_database != null) return _database;
    // lazily instantiate the db the first time it is accessed
    _database = await _initDatabase();
    return _database;
  }

  // this opens the database (and creates it if it doesn't exist)
  _initDatabase() async {
    String path = join(await getDatabasesPath(), _databaseName);
    return await openDatabase(path,
        version: _databaseVersion,
        onCreate: _onCreate);
  }




  // SQL code to create the database table
  Future _onCreate(Database db, int version) async {
    String dogTable = await DogTable().getInstance().then((value) => value.generateCreateTableQuery());
    await db.execute(dogTable);
  }



}
