import 'package:sqflite/sqflite.dart';
import 'package:sqflite_sample/DBHelper.dart';

abstract class BaseTable<T> extends DBHelper {
  String getTableName();

  Map<String, String> getFields();

  int getId(T t);

  Map<String, dynamic> toMap(T t);

  T fromMap(Map<String, dynamic> json);

  String generateCreateTableQuery() {
    String query;

    String tableName = getTableName();
    Map<String, String> fieldTypeMap = getFields();

    if (tableName.isNotEmpty &&
        fieldTypeMap != null &&
        fieldTypeMap.length != 0) {
      String createTableQuery = "CREATE TABLE " + tableName + "(";

      Iterable<String> fieldNames = fieldTypeMap.keys;
      bool isFirstIteration = true;

      for (String fieldName in fieldNames) {
        if (isFirstIteration) {
          isFirstIteration = false;
        } else {
          createTableQuery += ",";
        }

        String dataType = fieldTypeMap[fieldName].toString();
        createTableQuery += (fieldName + " " + dataType);
      }
      query = createTableQuery + ")";
    }
    return query;
  }

  insert(T t) async {
    final db = await database;
    final object = await findByID(getId(t));
    if (object == null) {
      var res = await db.insert(getTableName(), toMap(t),
          conflictAlgorithm: ConflictAlgorithm.replace);
      return res;
    }
  }

  update(T t) async {
    final db = await database;
    var res = await db.update(getTableName(), toMap(t),
        where: 'id = ?',
        whereArgs: [getId(t)],
        conflictAlgorithm: ConflictAlgorithm.replace);
    return res;
  }

  delete(T t) async {
    final db = await database;
    var res =
        await db.delete(getTableName(), where: 'id = ?', whereArgs: [getId(t)]);
    return res;
  }

  findByID(int id) async {
    final db = await database;
    var res = await db.query(getTableName(), where: "id = ?", whereArgs: [id]);
    return res.isNotEmpty ? fromMap(res.first) : null;
  }

  findAll() async {
    final db = await database;
    var res = await db.query(getTableName());
    List<T> list = res.isNotEmpty ? res.map((c) => fromMap(c)).toList() : null;
    return list;
  }
}

enum FieldType { text, autoincrement, integer, double }

String fieldType(FieldType field) {
  switch (field) {
    case FieldType.text:
      return "TEXT NOT NULL";
    case FieldType.autoincrement:
      return "INTEGER PRIMARY KEY AUTOINCREMENT";
    case FieldType.integer:
      return "INTEGER";
    case FieldType.double:
      return "DOUBLE";
    default:
      return "";
  }
}
